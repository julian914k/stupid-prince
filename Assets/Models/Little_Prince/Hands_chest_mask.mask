%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Hands_chest_mask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000000000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Base_bone
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/FootRoll_crtl.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/FootRoll_crtl.L/FootRoll_crtl.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/Heel_roll.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/Heel_roll.L/Toe_roll.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/Heel_roll.L/Toe_roll.L/Foot_IK.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/Heel_roll.L/Toe_roll.L/Foot_IK.L/Foot_IK.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/Knee_IK.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.L/Knee_IK.L/Knee_IK.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/FootRoll_crtl.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/FootRoll_crtl.R/FootRoll_crtl.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/Heel_roll.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/Heel_roll.R/Toe_roll.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/Heel_roll.R/Toe_roll.R/Foot_IK.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/Heel_roll.R/Toe_roll.R/Foot_IK.R/Foot_IK.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/Knee_IK.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Foot_ctrl.R/Knee_IK.R/Knee_IK.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hand_IK.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hand_IK.L/Elbow_IK.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hand_IK.L/Elbow_IK.L/Elbow_IK.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hand_IK.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hand_IK.R/Elbow_IK.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hand_IK.R/Elbow_IK.R/Elbow_IK.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_index_1.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_index_1.L/finger_index_2.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_index_1.L/finger_index_2.L/finger_index_3.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_index_1.L/finger_index_2.L/finger_index_3.L/finger_index_3.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_little_1.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_little_1.L/finger_little_2.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_little_1.L/finger_little_2.L/finger_little_3.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_little_1.L/finger_little_2.L/finger_little_3.L/finger_little_3.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_middle_1.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_middle_1.L/finger_middle_2.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_middle_1.L/finger_middle_2.L/finger_middle_3.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_middle_1.L/finger_middle_2.L/finger_middle_3.L/finger_middle_3.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_ring_1.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_ring_1.L/finger_ring_2.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_ring_1.L/finger_ring_2.L/finger_ring_3.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/finger_ring_1.L/finger_ring_2.L/finger_ring_3.L/finger_ring_3.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/Thumb_1.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/Thumb_1.L/Thumb_2.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.L/Upper_arm.L/Bottom_arm.L/Hand.L/Thumb_1.L/Thumb_2.L/Thumb_2.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_index_1.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_index_1.R/finger_index_2.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_index_1.R/finger_index_2.R/finger_index_3.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_index_1.R/finger_index_2.R/finger_index_3.R/finger_index_3.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_little_1.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_little_1.R/finger_little_2.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_little_1.R/finger_little_2.R/finger_little_3.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_little_1.R/finger_little_2.R/finger_little_3.R/finger_little_3.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_middle_1.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_middle_1.R/finger_middle_2.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_middle_1.R/finger_middle_2.R/finger_middle_3.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_middle_1.R/finger_middle_2.R/finger_middle_3.R/finger_middle_3.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_ring_1.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_ring_1.R/finger_ring_2.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_ring_1.R/finger_ring_2.R/finger_ring_3.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/finger_ring_1.R/finger_ring_2.R/finger_ring_3.R/finger_ring_3.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/Thumb_1.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/Thumb_1.R/Thumb_2.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Clavicle.R/Upper_arm.R/Bottom_arm.R/Hand.R/Thumb_1.R/Thumb_2.R/Thumb_2.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck/Head/eye.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck/Head/eye.L/eye.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck/Head/eye.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck/Head/eye.R/eye.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck/Head/jaw
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/spine_2/spine_2.1/spine_3/Neck/Head/jaw/jaw_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.L/Bottom_low_leg.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.L/Bottom_low_leg.L/Foot.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.L/Bottom_low_leg.L/Foot.L/Toe.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.L/Bottom_low_leg.L/Foot.L/Toe.L/Toe.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.R/Bottom_low_leg.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.R/Bottom_low_leg.R/Foot.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.R/Bottom_low_leg.R/Foot.R/Toe.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/spine_1/Upper_leg.R/Bottom_low_leg.R/Foot.R/Toe.R/Toe.R_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/tail.L
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/tail.L/tail.L_end
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/tail.R
    m_Weight: 1
  - m_Path: Armature/Base_bone/Hips_ctrl/tail.R/tail.R_end
    m_Weight: 1
  - m_Path: Armature/Weapon_bone
    m_Weight: 0
  - m_Path: Armature/Weapon_bone/Little_pr_sword
    m_Weight: 0
  - m_Path: Armature/Weapon_bone/Weapon_bone_end
    m_Weight: 0
  - m_Path: LIttle_prince
    m_Weight: 1
  - m_Path: LIttle_prince.001
    m_Weight: 1
  - m_Path: LIttle_prince_crown
    m_Weight: 1
