﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchPoint : MonoBehaviour 
{
	public string ItemKey;

	public GameObject Cylinder_INACTIVE;
	public GameObject Cylinder_ACTIVE;

	public bool Active
	{
		get
		{
			return Cylinder_ACTIVE.activeSelf;
		}
		set
		{
			Cylinder_ACTIVE.SetActive(value);
			Cylinder_INACTIVE.SetActive(!value);
		}
	}

	void Start()
	{
		Active = true;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			switch (ItemKey)
			{
				case "Flower" : 
				StageController.Instance.PrinceHandState = PrinceHandState.HasFlower;
				break;
				case "Weapon" :
				StageController.Instance.PrinceHandState = PrinceHandState.HasWeapon;
				break;
				case "Princess":
				// 꽃을 가지고 있을 때
				if (StageController.Instance.PrinceHandState == PrinceHandState.HasFlower)
				{
					// 곰이 살아 있으면 곰이 나타나는 컷씬을 보여주고 게임 오버.
					if (!StageController.Instance.IsDieBeast)
						StageController.Instance.PlayBeastAttackCutScene();
					// 곰이 죽어 있으면 공주에게 꽃을 준다.
					else
						StageController.Instance.IsGiveFlower = true;
				}
				// 공주에게 닿으면 게임 클리어.
				StageController.Instance.GameClear();
				break;
				case "Beast" :
				if (StageController.Instance.PrinceHandState == PrinceHandState.HasWeapon)
				{
					StageController.Instance.IsDieBeast = true;
					GetComponent<Animator>().Play("Death");
					StageController.Instance.Beast.SetActive(false);
				}
				else
					return;
				break;
			}
			Active = false;
		}
	}
}
