﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject playerObject = null;
    public float cameraTrackingSpeed = 0.2f;
    private Vector3 lastTargetPosition = Vector3.zero;
    private Vector3 currentTargetPosition = Vector3.zero;
    private float currentLerpDistance = 0.0f;

    void Start()
    {
        // Set the initial camera positioning to prevent any weird jerking

        // around
        Vector3 playerPos = playerObject.transform.position;
        Vector3 cameraPos = transform.position;
        Vector3 startTargetPos = playerPos;

        lastTargetPosition = startTargetPos;
        currentTargetPosition = startTargetPos;
        currentLerpDistance = 1.0f;
    }

    void LateUpdate()
    {
        trackPlayer();

        // Continue moving to the current target position
        currentLerpDistance += cameraTrackingSpeed;
        transform.position = Vector3.Lerp(lastTargetPosition, currentTargetPosition, currentLerpDistance);
    }

    void trackPlayer()
    {
        // Get and store the current camera position, and the current player position, in world coordinates.
        Vector3 currentCamPos = transform.position;
        Vector3 currentPlayerPos = playerObject.transform.position;

		currentPlayerPos.z -= 4.0f;
		currentPlayerPos.y += 2.0f;

        if (currentCamPos.x == currentPlayerPos.x && currentCamPos.y == currentPlayerPos.y)
        {
            // Positions are the same - tell the camera not to move, then abort.
            currentLerpDistance = 1f;
            lastTargetPosition = currentCamPos;
            currentTargetPosition = currentCamPos;
            return;
        }

        // Reset the travel distance for the lerp
        currentLerpDistance = 0f;

        // Store the current target position so we can lerp from it
        lastTargetPosition = currentCamPos;

        // Store the new target position
        currentTargetPosition = currentPlayerPos;
    }
}
