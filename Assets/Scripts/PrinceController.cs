﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class PrinceController : MonoBehaviour
{
    public float JumpPower = 16000f;
    public float RunSpeed = 3.0f;
    private Animator animator;
    private Rigidbody body;
    private Transform mainCameraTransform;
    public bool IsGrounded;
    public Transform GroundChecker;
    public float groundRadius = 0.1f;
    public LayerMask GroundLayer;
    private Collider[] CheckGround()
    {
        return Physics.OverlapSphere(GroundChecker.position, this.groundRadius, GroundLayer);
    }
    private void OnEnable()
    {
        mainCameraTransform = Camera.main.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(CheckGround().Length > 0){
            IsGrounded = true;
        }else{
            IsGrounded = false;
        }

        Vector3 inputVector = new Vector3(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
        Vector3 movementVector = Vector3.zero;

        if (VehicleController.Instance.AllowPrinceController)
        {
            if (Input.GetKeyDown("space") || CnInputManager.GetButtonDown("Jump")) //jump
            {
                if (IsGrounded)
                {
                    animator.SetTrigger("jump");
                    body.AddForce(new Vector3(0, JumpPower, 0));
                }
            }

            // If we have some input
            if (inputVector.sqrMagnitude > 0.001f)
            {
                movementVector = mainCameraTransform.TransformDirection(inputVector);
                movementVector.y = 0f;
                movementVector.Normalize();
                transform.forward = movementVector;
                Vector3 velocity3 = transform.forward * RunSpeed * Time.deltaTime;
                velocity3.y = body.velocity.y;
                body.velocity = velocity3;
                //animator.SetInteger("moving", 2);//run
            }

        }

        animator.SetFloat("velocityX", Mathf.Abs(body.velocity.x));
        animator.SetFloat("velocityZ", Mathf.Abs(body.velocity.z));
    }
}
