﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour
{
    public static StageManager Instance = null;
    [System.Serializable]
    public class Stage
    {
        public Stage(StageName stageName)
        {
            this.StageName = stageName;
            IsClearStage = false;
            IsKillBear = false;
            IsGiveFlower = false;
        }
        public StageName StageName;
        public bool IsClearStage;
        public bool IsKillBear;
        public bool IsGiveFlower;
    }

    public List<Stage> Stages;

    public Image Stage1Square;
    public Image Stage2Square;
    public Image Stage3Square;
    public Image Stage1Star;
    public Image Stage1Bear;
    public Image Stage1Heart;
    public Image Stage2Star;
    public Image Stage2Bear;
    public Image Stage2Heart;
    public Image Stage3Star;
    public Image Stage3Bear;
    public Image Stage3Heart;


    public StageName SelectedStageName;
    public Text StageTitle;
    public enum StageName
    {
        Stage1,
        Stage2,
        Stage3
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void Start()
    {

        Stage stage1 = new Stage(StageName.Stage1);
        //stage1.IsClearStage = true;
        Stage stage2 = new Stage(StageName.Stage2);
        Stage stage3 = new Stage(StageName.Stage3);
        Stages = new List<Stage>();
        Stages.Add(stage1);
        Stages.Add(stage2);
        Stages.Add(stage3);

        stage1.IsClearStage = PlayerPrefs.GetInt(StageName.Stage1.ToString() + "IsClearStage") != 0 ? true : false;
        stage1.IsKillBear = PlayerPrefs.GetInt(StageName.Stage1.ToString() + "IsKillBear") != 0 ? true : false;
        stage1.IsGiveFlower = PlayerPrefs.GetInt(StageName.Stage1.ToString() + "IsGiveFlower") != 0 ? true : false;

        stage2.IsClearStage = PlayerPrefs.GetInt(StageName.Stage2.ToString() + "IsClearStage") != 0 ? true : false;
        stage2.IsKillBear = PlayerPrefs.GetInt(StageName.Stage2.ToString() + "IsKillBear") != 0 ? true : false;
        stage2.IsGiveFlower = PlayerPrefs.GetInt(StageName.Stage2.ToString() + "IsGiveFlower") != 0 ? true : false;

        stage3.IsClearStage = PlayerPrefs.GetInt(StageName.Stage3.ToString() + "IsClearStage") != 0 ? true : false;
        stage3.IsKillBear = PlayerPrefs.GetInt(StageName.Stage3.ToString() + "IsKillBear") != 0 ? true : false;
        stage3.IsGiveFlower = PlayerPrefs.GetInt(StageName.Stage3.ToString() + "IsGiveFlower") != 0 ? true : false;


        foreach (Stage stage in Stages)
        {
            switch (stage.StageName)
            {
                case StageName.Stage1:
                    if ((stage.IsClearStage))
                        Stage1Star.enabled = false;
                    if (stage.IsKillBear)
                        Stage1Bear.enabled = false;
                    if (stage.IsGiveFlower)
                        Stage1Heart.enabled = false;
                    break;
                case StageName.Stage2:
                    if (stage.IsClearStage)
                        Stage2Star.enabled = false;
                    if (stage.IsKillBear)
                        Stage2Bear.enabled = false;
                    if (stage.IsGiveFlower)
                        Stage2Heart.enabled = false;
                    break;
                case StageName.Stage3:
                    if (stage.IsClearStage)
                        Stage3Star.enabled = false;
                    if (stage.IsKillBear)
                        Stage3Bear.enabled = false;
                    if (stage.IsGiveFlower)
                        Stage3Heart.enabled = false;
                    break;
            }

        }

        SelectedStageName = StageName.Stage1;
        switch (SelectedStageName)
        {
            case StageName.Stage1:
                Stage1Square.enabled = true;
                break;
            case StageName.Stage2:
                Stage2Square.enabled = true;
                break;
            case StageName.Stage3:
                Stage3Square.enabled = true;
                break;
        }


    }
    public void SetStage(string stageName)
    {
        SelectedStageName = (StageName)StageName.Parse(typeof(StageName), stageName);
        switch (SelectedStageName)
        {
            case StageName.Stage1:
                StageTitle.text = "계곡 탐험";
                Stage1Square.enabled = true;
                Stage2Square.enabled = false;
                Stage3Square.enabled = false;
                break;
            case StageName.Stage2:
                StageTitle.text = "화산섬";
                Stage1Square.enabled = false;
                Stage2Square.enabled = true;
                Stage3Square.enabled = false;
                break;
            case StageName.Stage3:
                StageTitle.text = "준비중";
                Stage1Square.enabled = false;
                Stage2Square.enabled = false;
                Stage3Square.enabled = true;
                break;
        }
    }
    public void StartStage()
    {
        SceneManager.LoadScene(SelectedStageName.ToString());
    }
}
