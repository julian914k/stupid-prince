﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed = 1.0f;
    public float RunSpeed = 3.0f;
    public float TurnSpeed = 90.0f;
	public float JumpPower = 100;

    public Transform HandPoint;

    private Animator anim;
    private Rigidbody body;
    private Vector3 moveDirection = Vector3.zero;
    private float walkSpeed = 0.0f;
    private float runSpeed = 0.0f;
    private bool isGrounded = true;
    private bool isJumping = false;

    private float turnAngle;

    void Start()
    {
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody>();

        walkSpeed = Speed;
        runSpeed = RunSpeed;

		turnAngle = transform.localEulerAngles.y;
    }

    void Update()
    {
        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");

        if (v > 0)
        {
            // 달리기 애니메이션 
            anim.SetInteger("moving", 2);
        }
        else if (v < 0)
        {
			// 뒤로 가기 애니메이션
            anim.SetInteger("moving", 22);
            runSpeed = walkSpeed / 2;
        }
        else
        {
			// 서있는 애니메이션
            anim.SetInteger("moving", 0);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
			// 점프 애니메이션
			anim.Play("Armature|LP_jump");

			body.AddForce(new Vector3(0, JumpPower, 0));
        }

        if (isGrounded)
        {
            float velocityY = body.velocity.y;

            moveDirection = transform.forward * v * Speed * runSpeed;
            moveDirection.y = velocityY;

            turnAngle += h * TurnSpeed * Time.deltaTime;
            body.MoveRotation(Quaternion.Euler(0, turnAngle, 0));
        }

        body.velocity = moveDirection;
    }
}
