﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;
using UnityEngine.UI;

public class VehicleController : MonoBehaviour
{
    public static VehicleController Instance = null;
    public Transform DetectPoint;
    public float PushPower = 100f;
    public float DetectRadius = 0.2f;
    public float RotateFactor = 0.01f;
    [Range(0, 360)]
    public float RoationLimitAngle = 45f;
    public bool AllowVehicleController = false;
    public bool AllowPrinceController = true;
    public bool AllowSwitchController = true;
    public SensitiveJoystick joystick;
    public Image PrinceButtonImage;
    public Image VehicleButtonImage;
    private Rigidbody body;
    private float rotY;
    public void InitControllerAuth()
    {
        AllowVehicleController = false;
        AllowPrinceController = true;
        AllowSwitchController = true;
    }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody>();

        rotY = transform.rotation.eulerAngles.y;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        foreach (Collider collider in Physics.OverlapSphere(DetectPoint.position, DetectRadius))
        {
            if (collider.tag == "Player")
            {
                AllowVehicleController = true;
                AllowPrinceController = false;
                AllowSwitchController = false;

                PrinceButtonImage.enabled = false;
                VehicleButtonImage.enabled = true;

                joystick.JoystickMoveAxis = ControlMovementDirection.Horizontal;
                break;
            }
            if (AllowSwitchController)
                AllowVehicleController = false;
        }

        if (AllowVehicleController)
        {
            Vector3 inputVector = new Vector3(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
            Vector3 movementVector = Vector3.zero;

            // If we have some input
            if (inputVector.sqrMagnitude > 0.001f)
            {
                rotY = Mathf.LerpAngle(rotY, RoationLimitAngle * (inputVector.x > 0 ? 1 : -1), RotateFactor);
                body.MoveRotation(Quaternion.Euler(transform.rotation.eulerAngles.x, rotY, transform.rotation.eulerAngles.z));
                body.AddForce(transform.forward * PushPower * Time.deltaTime);
            }

            if (Input.GetKeyDown("space") || CnInputManager.GetButton("Jump")) //jump
            {
                body.AddForce(transform.forward * PushPower * 150 * Time.deltaTime);
            }

        }
    }
}
