﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public enum PrinceHandState
{
    None,
    HasFlower,
    HasWeapon
}

public class StageController : MonoBehaviour
{
    private static StageController instance;

    public static StageController Instance
    {
        get
        {
            return instance;
        }
    }

    public PrinceHandState PrinceHandState
    {
        get
        {
            return princeHandState;
        }
        set
        {
            princeHandState = value;

            FlowerOnHand.SetActive(false);
            WeaponOnHand.SetActive(false);

            switch (princeHandState)
            {
                case PrinceHandState.None:
                    break;
                case PrinceHandState.HasFlower:
                    FlowerOnHand.SetActive(true);
                    break;
                case PrinceHandState.HasWeapon:
                    WeaponOnHand.SetActive(true);
                    break;
            }
        }
    }

    public bool IsClear = false;
    public bool IsGiveFlower = false;
    public bool IsDieBeast = false;

    [Header("Game Object")]
    public GameObject Prince;
    public GameObject Cart;
    public GameObject Beast;
    public GameObject FlowerOnHand;
    public GameObject WeaponOnHand;

    [Header("UI")]
    public GameObject GameClearWindow;
    public GameObject GameOverWindow;
    public GameObject MenuWindow;
    public Image RewardImage_Clear;         // 클리어 했을 때
    public Image RewardImage_GiveFlower;    // 공주에게 꽃을 주었을 때
    public Image RewardImage_DieBeast;      // 곰을 물리쳤을 때

    [Header("Cut Scene")]
    public PlayableDirector BeastAttackCutScene;

    private PrinceHandState princeHandState;
    private Vector3 princeStartPosition;
    private Vector3 cartStartPosition;
    private Quaternion princeStartRotation;
    private Quaternion cartStartRotation;

    private Rigidbody princeRigidBody;
    public bool checkGameOver = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        princeRigidBody = Prince.GetComponent<Rigidbody>();

        princeStartPosition = Prince.transform.position;
        cartStartPosition = Cart.transform.position;
        princeStartRotation = Prince.transform.rotation;
        cartStartRotation = Cart.transform.rotation;
    }

    void Update()
    {
        if (!checkGameOver)
        {
            if (IsGameOver())
            {
                Invoke("IsGameOver", 3.0f);
                checkGameOver = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart();
        }

        if(Input.GetKey (KeyCode.Escape))
        {
            MenuWindow.SetActive(true);
        }
    }

    public bool IsGameOver()
    {
        if (checkGameOver)
        {
            if (princeRigidBody.velocity.x <= 0 && princeRigidBody.velocity.y <= 0 && princeRigidBody.velocity.z <= 0)
            {
                GameOver();
            }
            else
            {
                checkGameOver = false;
            }
        }
        if (VehicleController.Instance.AllowVehicleController)
        {
            if (princeRigidBody.velocity.x <= 0 && princeRigidBody.velocity.y <= 0 && princeRigidBody.velocity.z <= 0)
            {
                return true;
            }
        }
        return false;
    }

    public void Restart()
    {
        if (Prince != null)
        {
            Prince.transform.position = princeStartPosition;
            Prince.transform.rotation = princeStartRotation;
        }
        if (Cart != null)
        {
            Cart.transform.position = cartStartPosition;
            Cart.transform.rotation = cartStartRotation;
        }
        VehicleController.Instance.InitControllerAuth();
        Prince.GetComponent<Rigidbody>().isKinematic = true;
        Cart.GetComponent<Rigidbody>().isKinematic = true;
        Invoke("ActiveCharacters", 0.1f);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void ActiveCharacters()
    {
        Prince.GetComponent<Rigidbody>().isKinematic = false;
        Cart.GetComponent<Rigidbody>().isKinematic = false;
    }

    public void GameClear()
    {
        IsClear = true;

        GameClearWindow.SetActive(true);

        StageManager.Stage stage = StageManager.Instance.Stages.Find( x => x.StageName == StageManager.Instance.SelectedStageName);
        
        stage.IsClearStage = IsClear;
        stage.IsGiveFlower = IsGiveFlower;
        stage.IsKillBear = IsDieBeast;

        RewardImage_Clear.enabled = IsClear;
        RewardImage_DieBeast.enabled = IsDieBeast;
        RewardImage_GiveFlower.enabled = IsGiveFlower;
    }

    public void GameOver()
    {
        GameOverWindow.SetActive(true);
    }

    public void LoadMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void PlayBeastAttackCutScene()
    {
        if (BeastAttackCutScene.state == PlayState.Playing)
            return;

        StartCoroutine(CheckBeastAttackCutScenePlayState());
    }

    IEnumerator CheckBeastAttackCutScenePlayState()
    {
        Vector3 initialPosition = Beast.transform.position;
        Quaternion initialRotation = Beast.transform.rotation;
        BeastAttackCutScene.gameObject.SetActive(true);

        yield return null;

        yield return new WaitUntil(() => BeastAttackCutScene.state == PlayState.Paused);

        Beast.transform.position = initialPosition;
        Beast.transform.rotation = initialRotation;
        BeastAttackCutScene.gameObject.SetActive(false);

        GameOver();
    }

    public void SetPrinceHandState(int handState)
    {
        PrinceHandState = (PrinceHandState)handState;
    }
}
